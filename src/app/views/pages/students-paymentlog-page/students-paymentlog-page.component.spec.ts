import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsPaymentlogPageComponent } from './students-paymentlog-page.component';

describe('StudentsPaymentlogPageComponent', () => {
  let component: StudentsPaymentlogPageComponent;
  let fixture: ComponentFixture<StudentsPaymentlogPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsPaymentlogPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsPaymentlogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
