import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'kt-students-paymentlog-page',
  templateUrl: './students-paymentlog-page.component.html',
  styleUrls: ['./students-paymentlog-page.component.scss']
})

export class StudentsPaymentlogPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window["studentsPaymentlogPage"].init();
  }
}