import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'kt-channels-page',
  templateUrl: './channels-page.component.html',
  styleUrls: ['./channels-page.component.scss']
})

export class ChannelsPageComponent implements OnInit {
  constructor() { }

  ngOnInit() {
    window["channelsPage"].init();
  }
}