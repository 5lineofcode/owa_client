
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

declare var $: any;

@Component({
	selector: 'kt-customers-list',
	templateUrl: './customers-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CustomersListComponent implements OnInit {
	constructor() { }

	openConfirmationModal() {
		$("#confirmationModal").modal('show');
	}

	ngOnInit() {
		$('#example').DataTable({
			processing: true,
			serverSide: true,
			ajax: "http://localhost/backend/public/index.php/users",
			columns: [
				{ data: 'user_id', name: 'user_id' },
				{ data: 'first_name', name: 'first_name' },
				{ data: 'last_name', name: 'last_name' },
				{
					data: 'email',
					name: 'email'
				},
				{
					orderable: false,
					searchable: false,
					render: function (data, type, row) {
						var ya = this;

						return `
							<button class='btn btn-xs btn-warning row-edit' data-id="${row[0]}><i class="fas fa-edit></i> Edit</button>
							<button class='btn btn-xs btn-sm btn-danger'><i class="fas fa-trash"></i> Delete</button>
							<button class='btn btn-xs btn-info'><i class="fas fa-money-bill"></i> Subscriptions</button>
							<button class='btn btn-xs btn-primary'>Test</button>
							<button class='btn btn-xs mat-raised-button'><i class="fas fa-money-bill"></i></button>
						`;
					}
				}
			],
		});

		$(document).off();
		$(document).on("click", ".row-edit", (e) => {
			var id = $(this).attr("data-id");
			alert("NiceOne > " + id);
		});
	}
}
