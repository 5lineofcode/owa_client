class UsersPage {

    init() {
        iniCustomUI();

        usersTable.DataTable({
            ajax: `${baseUrl}/firebase/users`,
            columns: [
                {
                    data: 'uid',
                    name: 'uid',
                    render: function (data, type, row) {
                        return `<a href='#' class='show-user-detail' email='${row.email}'>${data}</a>`;
                    }
                },
                {
                    data: 'providerData',
                    name: 'lastLoginAt',
                    render: function (data, type, row) {
                        var template = "";
                        for (var i = 0; i < data.length; i++) {
                            var providerId = data[i].providerId;


                            if (providerId == "password") {
                                template += `
                                    <div>
                                        <img src='http://icons.iconarchive.com/icons/paomedia/small-n-flat/16/key-icon.png'/> Password<br/>
                                    </div>
                                `;
                            }
                            else if (providerId == "google.com") {
                                template += `
                                    <div>
                                        <img src='http://icons.iconarchive.com/icons/google/chrome/16/Google-Chrome-icon.png'/> Google<br/>
                                    </div>
                                `;
                            }
                            else if (providerId == "facebook.com") {
                                template += `
                                    <div>
                                        <img src='http://icons.iconarchive.com/icons/emey87/social-button/16/facebook-icon.png'/> Facebook<br/>
                                    </div>
                                `;
                            }
                        }

                        var anonymous_template = `
                            <div>
                                <img src='http://icons.iconarchive.com/icons/pixelmixer/basic-2/16/user-anonymous-icon.png'/> Anonymous
                            </div>
                        `;

                        return data.length > 0 ? template : anonymous_template;
                    }
                },
                {
                    data: 'email',
                    name: 'email',
                    render: function (data, type, row) {
                        return data == null ? "N/A" : data;
                    }
                },
                {
                    data: 'displayName',
                    name: 'displayName',
                    render: function (data, type, row) {
                        return data == null ? "N/A" : data;
                    }
                },
                {
                    data: 'metadata',
                    name: 'createdAt',
                    render: function (data, type, row) {
                        return data.createdAt;
                    }
                },
                {
                    data: 'metadata',
                    name: 'lastLoginAt',
                    render: function (data, type, row) {
                        return data.lastLoginAt;
                    }
                },
                {
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        var id = row.uid;

                        return `
                            <span>
                                <i class="fas fa-trash row-delete row-action color-red" rowid="${id}"></i>
                            </span>
                        `;
                    }
                }
            ],
        });

        selectedLastLoginDate.val(new Date().toDateInputValue());

        deleteOldAnonymousUserBtn.click(function () {
            //reset button and hide alert
            $(this).parent().find("button").show();
            closeDeleteOldAnonymousUserBtn.hide();
            deleteOldAnonymousUserDialogInfo.hide();

            deleteOldAnonymousUserDialog.showDialog();
        });

        deleteOldAnonymousUserDialogConfirmationBtn.click(function () {
            var rows = usersTable.DataTable().rows().data();
            var input_date = Date.parse(selectedLastLoginDate.val(), "Y-m-d");


            //!QueQue it 1st
            var quequeUsers = [];
            for (var key in rows) {
                var row = rows[key];

                if (typeof row.metadata === 'undefined')
                    continue;

                var lastLoginAt = Date.parse(row.metadata.lastLoginAt.substring(0, 10), "Y-m-d");

                if (lastLoginAt <= input_date && row.providerData.length == 0) {
                    quequeUsers.push(row.uid);
                }
            }

            //! Do the job
            var allDataCount = quequeUsers.length;
            var quequeDataCount = quequeUsers.length;
            var index = 0;

            //hide close button
            $(this).parent().find("button").hide();
            closeDeleteOldAnonymousUserBtn.show();

            var myInterval = setInterval(function () {
                var uid = quequeUsers[index];
                index++;
                quequeDataCount--;

                //!Check if dialog closed or not
                if (!deleteOldAnonymousUserDialog.is(':visible')) {
                    deleteOldAnonymousUserDialog.hideDialog();
                    clearInterval(myInterval);
                    usersTable.DataTable().ajax.reload();
                    return;
                }

                $.ajax({
                    url: `${baseUrl}/firebase/users/${uid}`,
                    method: "DELETE"
                }).done(function (response) {
                    deleteOldAnonymousUserDialogInfo.show().html(`Current Status : <b>(${quequeDataCount}/${allDataCount})</b>`);
                });

                if (quequeDataCount == 0) {
                    deleteOldAnonymousUserDialog.hideDialog();
                    clearInterval(myInterval);

                    setTimeout(function () {
                        usersTable.DataTable().ajax.reload();
                    }, 1000);
                }
            }, 1000);
        });

        simulate10AnonymousLogin.click(async function () {
            showMessage({
                title: "Simulate Data~",
                message: "Please wait for a few seconds, we will create 10 Anonymous users for you!",
                alertType: "success developer",
                disableClose: true
            });

            for (var i = 0; i < 10; i++) {
                await firebase.auth().signOut();
                await firebase.auth().signInAnonymously().catch(function (error) {
                    console.log(error);
                });
            }

            setTimeout(function () {
                usersTable.DataTable().ajax.reload();
                usersTable.DataTable().order([1, 'asc']).draw();
                $("#tempDialog").hideDialog();
            }, 2000);
        });

        filterUserTypeBtn.click(function () {
            filterUserDialog.showDialog();
        });

        $(".filter-user-item").click(function (e) {
            e.preventDefault();
            var value = $(this).attr("value");

            var url = `${baseUrl}/api/users/get-list-user-type`;
            $.post(url, {
                "user_type": value
            }).done(function (response) {

                //update header title
                var headerTextEL = $(".kt-portlet__head-title");
                var headerTitle = headerTextEL.html().split(" - ")[0];
                headerTextEL.html(`${headerTitle} - <span style='color: green; font-weight: bold;'>${value}</span>`);

                usersTable
                    .DataTable()
                    .column(2)
                    .search(response, true, false).draw();

                filterUserDialog.hideDialog();
            });

        });

        clearUserFilterBtn.click(function () {

            //update header title to default
            var headerTextEL = $(".kt-portlet__head-title");
            var headerTitle = headerTextEL.html().split(" - ")[0];
            headerTextEL.html(`${headerTitle}`);

            usersTable
                .DataTable()
                .search('')
                .columns().search('')
                .draw();

            filterUserDialog.hideDialog();
        });

        $(".show-user-detail").off();
        $(document).on("click", ".show-user-detail", function (e) {
            e.preventDefault();
            var email = $(this).attr("email");

            if (email == "null") {
                showMessage({
                    title: "No Detail",
                    message: "Anonymous User doesn't have any Detail",
                    alertType: "danger"
                });
            }
            else {
                var url = `${baseUrl}/api/users/get-user-type`;
                $.post(url, {
                    "email": email
                }).done(function (response) {
                    var user = JSON.parse(response);
                    usersPage.showUsersDetail(user);
                });
            }
        });
    }

    showUsersDetail(user) {
        if (typeof user.error_type !== 'undefined') {
            showMessage({
                title: "No Data",
                message: user.message,
                alertType: "danger"
            });
            return;
        }

        //hide users detail
        $(".detail_info").hide();

        switch (user.user_type) {
            case "Student":
                $("#student_detail").show();
                break;
            case "Teacher":
                $("#teacher_detail").show();
                break;
        }

        window["mode"] = "edit";
        window["selectedId"] = user.user_id;
        usersPage.showEditUserForm();
    }

    onCreateBtnClick() {
        formDialog.find(".modal-title").html("Create Data");
        formDialog.find("form").trigger("reset");
        formDialog.showDialog();
    }

    onDeleteConfirmationBtnClick() {
        var url = `${baseUrl}/api/users/${selectedId}`;
        deleteConfirmationDialog.modal("hide");
        $.ajax({
            url: url,
            type: 'DELETE',
            success: function (result) {
                setTimeout(function () {
                    usersTable.DataTable().ajax.reload();
                }, 200);
            }
        });
    }

    showEditUserForm() {
        var id = window["selectedId"];
        formDialog.find(".modal-title").html(`Update Data #${id}`);

        var url = `${baseUrl}/api/users/${id}`;
        $.get(url).done(function (response) {
            var data = JSON.parse(response)[0];


            first_name.val(data.first_name);
            last_name.val(data.last_name);
            email.val(data.email);
            mobile_country_code.val(data.mobile_country_code);
            mobile.val(data.mobile);
            country.val(data.country);
            address.val(data.address);
            user_type.val(data.user_type);

            // $("#photo").val(data.photo);
            var photo_source = data.photo == null ? "assets/media/photo/no-photo.png" : `${baseUrl}/../${data.photo}`;
            photo.parent().find("img").attr("src", photo_source);
            window["photo_value"] = data.photo;

        });

        formDialog.showDialog();
    }

    onCreateFormSubmit() {
        var url = `${baseUrl}/api/users`;
        $.post(url, {
            first_name: first_name.val(),
            last_name: last_name.val(),
            email: email.val(),
            mobile_country_code: mobile_country_code.val(),
            mobile: mobile.val(),
            country: country.val(),
            address: address.val(),
            user_type: user_type.val(),
            photo: typeof photo_value === 'undefined' ? '' : photo_value
        }).done(function (data) {
            setTimeout(function () {
                usersTable.DataTable().ajax.reload();
            }, 500);
        });
    }

    onUpdateFormSubmit() {
        var url = `${baseUrl}/api/users/${selectedId}`;
        $.ajax({
            url: url,
            type: 'PUT',
            data: {
                first_name: first_name.val(),
                last_name: last_name.val(),
                email: email.val(),
                mobile_country_code: mobile_country_code.val(),
                mobile: mobile.val(),
                country: country.val(),
                address: address.val(),
                user_type: user_type.val(),
                photo: typeof photo_value === 'undefined' ? '' : photo_value
            },
            dataType: "json",
            success: function (result) {
                setTimeout(function () {
                    usersTable.DataTable().ajax.reload();
                }, 500);
            }
        });
    }
}

registerLogic(new UsersPage());