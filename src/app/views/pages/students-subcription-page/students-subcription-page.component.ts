import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'kt-students-subcription-page',
  templateUrl: './students-subcription-page.component.html',
  styleUrls: ['./students-subcription-page.component.scss']
})

export class StudentsSubcriptionPageComponent implements OnInit {
  constructor() { }

  ngOnInit() {
    window["studentsSubscriptionPage"].init();
  }
}