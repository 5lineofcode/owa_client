import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsSubcriptionPageComponent } from './students-subcription-page.component';

describe('StudentsSubcriptionPageComponent', () => {
  let component: StudentsSubcriptionPageComponent;
  let fixture: ComponentFixture<StudentsSubcriptionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsSubcriptionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsSubcriptionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
