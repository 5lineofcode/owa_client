window["getRestCountries"] = async function () {
    var result = await $.get("https://restcountries.eu/rest/v2/all");
    var data = result;

    $("#country").html("");
    $("#mobile_country_code").html("");
    for (var key in data) {
        var row = data[key];
        $("#country").append(`<option value='${row.name}'>${row.name}</option>`);
        $("#mobile_country_code").append(`<option value='+${row.callingCodes}'>${row.name} +${row.callingCodes}</option>`);
    }
}

window["initEasySelector"] = function(){
    $("*[id]").each(function(){
        var id = $(this).attr("id");
        window[id] = $(this);
    });
}

window["iniCustomUI"] = function () {
    window["initEasySelector"]();
    window["getRestCountries"]();
}

window["initRegisterUiData"] = function () {
    window["getRestCountries"]();
}

$(document).on("click", "#submit-register-btn", async function (e) {
    e.preventDefault();

    var email = $("#email").val();
    var password = $("#password").val();

    $.post(`${baseUrl}/api/users`, {
        first_name: $("#first_name").val(),
        last_name: $("#last_name").val(),
        email: email,
        password: password,
        mobile: $("#mobile").val(),
        mobile_country_code: $("#mobile_country_code").val(),
        country: $("#country").val(),
        address: $("#address").val(),
        photo: typeof photo_value === 'undefined' ? "" : photo_value
    }).done(async function (data) {
        console.log(data);
        $("#register-form").hide();
        // $("#need-activation-info-for-guardian").show();
        $("#need-activation-info-for-teacher").show();

        await firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
        });

    });

});