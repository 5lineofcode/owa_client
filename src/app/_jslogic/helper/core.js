$.fn.showDialog = function () {
    $(this).appendTo("body");
    $(this).modal('show');
}

$.fn.hideDialog = function () {
    $(this).modal('hide');
}

$.fn.getSection = function () {
    return $(this).parent().closest("section").attr("controller");
}

String.prototype.toCamelCase = function () {
    return this.replace(/^([A-Z])|\s(\w)/g, function (match, p1, p2, offset) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();
    });
};

Date.prototype.toDateInputValue = (function () {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
});

window["registerLogic"] = function (obj) {
    var className = obj.constructor.name.toCamelCase();
    window[className] = obj;
}

//! Alert Helper
window["showMessage"] = function (obj) {

    var default_image = 'http://icons.iconarchive.com/icons/google/noto-emoji-smileys/72/10010-smiling-face-with-smiling-eyes-icon.png';
    var danger_image = 'http://icons.iconarchive.com/icons/google/noto-emoji-smileys/72/10052-frowning-face-icon.png';
    var info_image = 'http://icons.iconarchive.com/icons/google/noto-emoji-smileys/72/10082-smiling-face-with-halo-icon.png';
    var primary_image = 'http://icons.iconarchive.com/icons/google/noto-emoji-smileys/72/10002-beaming-face-with-smiling-eyes-icon.png';

    var developer_image = 'http://icons.iconarchive.com/icons/streamlineicons/streamline-ux-free/128/hacker-icon.png';


    if (typeof obj.alertType !== 'undefined') {
        switch (obj.alertType) {
            case "danger":
                default_image = danger_image;
                break;
            case "info":
                default_image = info_image;
                break;
            case "primary":
                default_image = primary_image;
                break;
        }

        //! Custom Image
        if (obj.alertType.indexOf("developer") > -1) {
            default_image = developer_image;
        }
    }


    obj.alertType = typeof obj.alertType === 'undefined' ? 'success' : obj.alertType;
    obj.disableClose = typeof obj.disableClose === 'undefined' ? false : obj.disableClose;

    $("#tempDialog").remove();
    var html = `
        <div id="tempDialog" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">${obj.title}</h5>
                ${obj.disableClose == false ? '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' : ''}
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p class="text-center">
                        <img src="${default_image}" class="mb-2"/>
                        <span class="alert alert-${obj.alertType}" style="text-align: left !important;">${obj.message}</span>
                    </p>
                </div>
                <div class="modal-footer">
                    ${obj.disableClose == false ? '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' : ''}
                </div>
            </div>
            </div>
        </div>
    `;
    $("body").prepend(html);

    $("#tempDialog").showDialog();
}