$(document).on("click", ".create-btn", function (e) {
    e.preventDefault();
    var section = $(this).getSection();
    var controller = window[section];

    window["mode"] = "create";
    controller.onCreateBtnClick();
});

$(document).on("click", ".delete-confirmation-btn", function (e) {
    e.preventDefault();
    var section = $(this).getSection();
    var controller = window[section];

    controller.onDeleteConfirmationBtnClick();
})

$(document).on("click", ".row-edit", function (e) {
    var id = $(this).attr('rowid');
    var section = $(this).getSection();
    var controller = window[section];

    window["mode"] = "edit";
    window["selectedId"] = id;
    controller.onRowEditClick();
});

$(document).on("click", ".row-delete", function (e) {
    e.preventDefault();
    var id = $(this).attr('rowid');
    var section = $(this).getSection();
    var controller = window[section];

    window["selectedId"] = id;
    $("#deleteConfirmationDialog").showDialog();
});

$(document).on("click", "#saveForm", function (e) {
    e.preventDefault();
    var section = $(".create-btn").getSection();
    var controller = window[section];

    $("#formDialog").modal("hide");
    var mode = window["mode"];
    if (mode == "create") {
        controller.onCreateFormSubmit();
    }
    else if (mode == "edit") {
        controller.onUpdateFormSubmit();
    }
});