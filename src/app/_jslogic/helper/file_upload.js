//! File Upload Script
function readURL(input, el) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            el.parent().find("img").attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function submitFile(el) {
    var fd = new FormData();
    fd.append('photo', el[0].files[0]);

    $.ajax({
        url: `${baseUrl}/photo-upload`,
        method: "POST",
        data: fd,
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            window[el.attr("id")+"_value"] = data.photo_location;
        }
    })
}

$(document).on("change", ".file-browse", function (e) {
    readURL(this, $(this));
    submitFile($(this));
});

$(document).on("click", ".file-upload-form img", function () {
    $(this).parent().find(".file-browse").trigger("click");
});