window["onLoginByEmailBtnClick"] = async function (email, password) {

    window["error"] = false;
    await firebase.auth().signInWithEmailAndPassword(email, password)
        .catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            window["login_status"] = "failed";
            window["error_message"] = errorMessage;
            window["error"] = true;
        });

    if (window["error"] == false) {
        window["login_status"] = "success";
        window["login_data"] = firebase.auth().currentUser;
    }
}


window["onLoginByGoogleBtnClick"] = async function () {

    await firebase.auth().signOut().then(async function () {

        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        await firebase.auth().signInWithPopup(provider).then(function (result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            window["login_status"] = "success";
            window["login_data"] = user;
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            window["login_status"] = "failed";
            window["error_message"] = errorMessage;
        });

    });
}

window["onLoginByFacebookBtnClick"] = async function () {

    await firebase.auth().signOut().then(async function () {

        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('user_birthday');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            window["login_status"] = "success";
            window["login_data"] = user;
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            window["login_status"] = "failed";
            window["error_message"] = errorMessage;
        });
    });
}